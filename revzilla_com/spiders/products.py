# -*- coding: utf-8 -*-
import re
import copy
import scrapy
from scrapy.spiders import SitemapSpider


class ProductsSpider(SitemapSpider):
    name = 'products'
    allowed_domains = ['revzilla.com']
    sitemap_urls = ['https://www.revzilla.com/sitemap.xml']
    sitemap_rules = [('motorcycle', 'parse')]


    def parse(self, response):
        d = {}

        d['title'] = response.css('h1.product-show-details-name__name ::text').extract_first()
        if d['title']:
            d['title'] = d['title'].strip()

        d['url'] = response.url

        # d['retail_price'] = response.css('div.product-details__price-retail ::text').extract_first()
        # if d['retail_price']:
        #     d['retail_price'] = re.sub('[^\d\.\,]', '', d['retail_price'].strip().split('-').pop(0))

        categories = list(filter(None, [s.strip() for s in response.css('nav.breadcrumbs ol li a[itemprop="item"] ::text').extract() if s]))
        if categories:
            d['category'] = ' > '.join(categories)
        else:
            d['category'] = ''

        d['image_urls'] = response.css('div.product-show-media-image__thumbnail-image ::attr(data-src)').extract()

        d['id'] = response.css('span.product-show-details-name__id ::text').extract_first()
        if d['id']:
            d['id'] = d['id'].replace('Item:', '').strip()

        d['short_descr'] = response.css('p.product-show-details-teaser__text ::text').extract_first()
        if d['short_descr']:
            d['short_descr'] = d['short_descr'].strip()

        d['long_descr'] = response.css('div.product-description__sku').extract_first()

        if response.css('div.tab-content[data-tab-id="fitment"]'):
            d['fitment'] = self.get_fitment(response)
        else:
            d['fitment'] = {}

        if not response.css('div.tab-content[data-tab-id="part-numbers"]'):
            d['style'] = ''
            d['revzilla_sku'] = ''
            d['mfr_sku'] = ''

            return d

        css = 'div.tab-content[data-tab-id="part-numbers"] tr.sku-data__body-row'
        trs = response.css(css)

        for tr in trs:
            tds = list(filter(None, [s.strip() for s in tr.xpath('td/text()').extract()]))

            dc = copy.deepcopy(d)

            dc['style'] = tds[0]
            dc['revzilla_sku'] = tds[1]
            dc['mfr_sku'] = tds[2]

            yield dc


    def get_fitment(self, response):
        d = {}

        groups = response.css('dl.product-show-fitment__vehicle-make-model')
        for group in groups:
            mfr_css = 'dt.product-show-fitment__vehicle-make span.product-show-fitment__vehicle-make-name ::text'
            mfr = group.css(mfr_css).extract_first()

            if mfr not in d:
                d[mfr] = {}

            dds = group.css('dd.product-show-fitment__vehicle-model')
            for dd in dds:
                model = list(filter(None, [s.strip() for s in dd.xpath('text()').extract() if s]))
                model = ''.join(model)

                if not model:
                    model = dd.css('a ::text').extract_first()
                    if model:
                        model = model.strip()

                years = list(filter(None, [s.strip() for s in dd.css('span.product-show-fitment__vehicle-model-years ::text').extract() if s]))
                years = ''.join(years)
                years = list(hyphen_range(years))

                d[mfr][model] = years

        return d


def hyphen_range(s):
    """ yield each integer from a complex range string like "1-9,12, 15-20,23"

    >>> list(hyphen_range('1-9,12, 15-20,23'))
    [1, 2, 3, 4, 5, 6, 7, 8, 9, 12, 15, 16, 17, 18, 19, 20, 23]

    >>> list(hyphen_range('1-9,12, 15-20,2-3-4'))
    Traceback (most recent call last):
        ...
    ValueError: format error in 2-3-4
    """
    for x in s.split(','):
        elem = x.split('-')
        if len(elem) == 1: # a number
            yield int(elem[0])
        elif len(elem) == 2: # a range inclusive
            start, end = map(int, elem)
            for i in range(start, end+1):
                yield i
        else: # more than one hyphen
            raise ValueError('format error in %s' % x)
